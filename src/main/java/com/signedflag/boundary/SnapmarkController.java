package com.signedflag.boundary;

import com.signedflag.account.AccountRepository;
import com.signedflag.account.Account;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
public class SnapmarkController {

    @Autowired
    private AccountRepository accountRepository;

    @RequestMapping("/snapmark/{name}")
    public List<Account> getUser(@PathVariable String name) {
        return accountRepository.findAll();
    }

}
