package com.signedflag.account;

import javax.persistence.*;

@Entity
@Table(name = "benutzer")
public class Account {

    @Id
    @GeneratedValue
    private Long id;

    @Column(name = "name")
    private String username;

    public String getUsername() {
        return username;
    }

    public void setUsername(String username) {
        this.username = username;
    }
    
}
