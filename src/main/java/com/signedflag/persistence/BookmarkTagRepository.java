package com.signedflag.persistence;

import com.signedflag.model.BookmarkTag;
import org.springframework.data.jpa.repository.JpaRepository;

public interface BookmarkTagRepository extends JpaRepository<BookmarkTag, Long> {
}
