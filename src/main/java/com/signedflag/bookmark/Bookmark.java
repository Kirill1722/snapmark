package com.signedflag.bookmark;

import com.signedflag.account.Account;

import javax.persistence.*;

@Entity
@Table(name = "bookmark")
public class Bookmark {

    @Id
    @GeneratedValue
    private Long id;

    @ManyToOne
    @JoinColumn(name="fk_benutzer_id")
    private Account account;

    @Column(name = "url")
    private String url;

    public Bookmark() {

    }

    public Bookmark(Account account, String url) {
        this.account = account;
        this.url = url;
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public Account getAccount() {
        return account;
    }

    public void setAccount(Account account) {
        this.account = account;
    }

    public String getUrl() {
        return url;
    }

    public void setUrl(String url) {
        this.url = url;
    }

}
