CREATE TABLE bookmark_tag (
  id bigint(20) NOT NULL AUTO_INCREMENT,
  name varchar(255) NOT NULL,
  fk_benutzer_id bigint(20) NOT NULL,
  fk_bookmark_id bigint(20) NOT NULL,
  PRIMARY KEY (id),
  UNIQUE KEY u_bookmark_tag (fk_benutzer_id, fk_bookmark_id, name)
) ENGINE=InnoDB AUTO_INCREMENT=1 DEFAULT CHARSET=utf8;
