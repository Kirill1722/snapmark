snapmark.config(function ($routeProvider) {

    $routeProvider

        .when('/', {
            templateUrl: 'pages/home.html',
            controller: 'homeController'
        })

        .when('/bookmark', {
            templateUrl: 'pages/bookmark.html',
            controller: 'bookmarkController'
        })

});