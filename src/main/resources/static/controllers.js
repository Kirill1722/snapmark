snapmark.controller('homeController', ['$scope', '$http', 'keywordsService', function ($scope, $http, keywordsService) {

    $scope.keywordsSearch = '';

    $scope.$watch('keywordsSearch', function () {

        $http.post('/user/bookmarksearch', '[' + '"' + $scope.keywordsSearch + '"' + ']')
            .success(function (result) {
                $scope.keywordsSearchResults = result;
            })
            .error(function (data, status) {
                alert("failure message: " + JSON.stringify({data: data}));
            });
    });

}]);

snapmark.controller('bookmarkController', ['$scope', '$http', '$routeParams', 'keywordsService', function ($scope, $http, $routeParams, keywordsService) {

    $scope.keywords = keywordsService.keywords;

    var res = $http.post('/user/bookmarksearch', '[' + '"' + $scope.keywords + '"' + ']');
    res.success(function (data, status, headers, config) {
        $scope.keywordsResult = data;
    });
    res.error(function (data, status, headers, config) {
        alert("failure message: " + JSON.stringify({data: data}));
    });

}]);
